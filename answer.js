/**
 * Created by Judith on 15-6-2016.
 */

decisionMaker.directive('answer', function () {
    return {
        link: function (s, e, a) {
            
        },
        template: '<div class = "input-group col-lg-offset-3 col-lg-6">     <span class="input-group-addon" ><span class="glyphicon glyphicon-star"> </span></span>   <input type = "text" class = "form-control" ng-model="x.answer_string" placeholder="Fill in an option">      <span class = "input-group-btn">        <button class = "btn btn-danger" type = "button" ng-click="deleteField($index)"><span class="glyphicon glyphicon-minus" </span></button> </span> </div><!-- /input-group -->'
        //template: ' <div class="input-group col-lg-offset-3 col-lg-6"> <span class="input-group-addon" ><span class="glyphicon glyphicon-star"> </span></span></span> <input type="text" class="form-control" placeholder="Write an option!" aria-describedby="basic-addon1"><div class="input-group-addon"><button type="button" class="input-group-addon btn btn-danger"><span class="glyphicon glyphicon-minus"></span></button></div></div>'
    }
});

decisionMaker.directive('answers', function(){
    var template = '';
    return {
        link: function (s, e, a) {
            for(var i = 0; i<a.n;i++){
                template += "<answer></answer>"
            }
            console.log(template);
            e.text(template);
            s.$apply();
        }
        //template: '<h1> test </h1>'
    }
})
