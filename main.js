/**
 * Created by Judith on 15-6-2016.
 */

var decisionMaker = angular.module('DecisionMaker', [])
    .controller('DmCtrl', function ($scope) {
        $scope.answers = [{},{}];
        $scope.decisionHasBeenMade = false;
        $scope.question = "";
        $scope.addField = function () {
        $scope.answers.push({})
        };

        $scope.deleteField = function (field) {
            console.log(field);
            if($scope.answers.length > 2){
                $scope.answers.splice(field, 1);
            }
        }

        $scope.makeDecision = function() {
            $scope.decisionHasBeenMade = true;

        };

        $scope.getRandomIndex = function(){
            return Math.floor((Math.random() * $scope.answers.length));
        }
        $scope.restart = function(){
            $scope.answers = [{},{}];
            $scope.question = "";
            $scope.decisionHasBeenMade = false;
        }
    });
